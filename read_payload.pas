program read_payload;
{$MODE DELPHI}
uses 
	SysUtils;

var
	inputFilePath: String;
	payload: String;

function ReadPayload(inputFilePath: String): String;
var
	inputFile: File of Byte;
	byteFromInput: Byte;
	charFromInput: Char;
	buffer: Array of Byte;
	payload: AnsiString;
	i: Cardinal;
begin
	
	// Check input file path not empty
	if Length(inputFilePath) = 0 then
	begin
		raise Exception.Create('Input file path is empty');
	end;

	// Check input file exists
	if not FileExists(inputFilePath) then 
	begin
		raise Exception.Create('File "' + inputFilePath + '" not found!');
	end;

	// Init buffer
	SetLength(buffer, 32);

	// Open input file and ready each byte
	AssignFile(inputFile, inputFilePath);
	Reset(inputFile);
	payload := '';
	// Read each char from the file
	while not Eof(inputFile) do
	begin
		Read(inputFile, byteFromInput);
		// Search payload begin signature
		charFromInput := Chr(byteFromInput);
		if charFromInput = '!' then
		begin
			Read(inputFile, byteFromInput);
			charFromInput := Chr(byteFromInput);
			if charFromInput = 'P' then
			begin
				Read(inputFile, byteFromInput);
				charFromInput := Chr(byteFromInput);
				if charFromInput = 'L' then
				begin
					Read(inputFile, byteFromInput);
					charFromInput := Chr(byteFromInput);
					if charFromInput = 'D' then
					begin
						Read(inputFile, byteFromInput);
						charFromInput := Chr(byteFromInput);
						if charFromInput = ':' then
						begin
							// Payload begin signature found, read paylod until eof or zero-char
							i := 0;
							while not Eof(inputFile) and (charFromInput <> #0) and (i < Length(buffer)) do
							begin
								
								Read(inputFile, byteFromInput);
								buffer[i] := byteFromInput;
								Inc(i);

								// Check array enough size
								if i > Length(buffer) - 1 then
								begin
									// Not enough, enlarge
									SetLength(buffer, Length(buffer) * 2);
								end;

							end;
							SetString(payload, PAnsiChar(@buffer[0]), i);
						end;
					end;
				end;
			end;
		end;
	end;
	CloseFile(inputFile);

	result := payload;
end;

begin
	
	if ParamCount < 1 then 
	begin
		WriteLn('Usage: read_payload input_file_path');
		halt;
	end;

	inputFilePath := ParamStr(1);
	payload := ReadPayload(inputFilePath);

	WriteLn('A payload value: ' + payload);

end.