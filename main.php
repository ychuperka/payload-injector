<?php
require_once __DIR__ . '/payload.php';

if ($argc != 4 && $argc != 3) {
    echo 'Syntax error! Usage:'. PHP_EOL;
    echo 'Write: php main.php input_file payload_base64_string output_file' . PHP_EOL;
    echo 'Read: php main.php input_file payload_output_file' . PHP_EOL;
    exit(-1);
}

if ($argc == 4) {

    $inputFile = $argv[1];
    $payload = $argv[2];
    $outputFile = $argv[3];

    if (write_payload($payload, $inputFile, $outputFile)) {
        echo 'Success!' . PHP_EOL;
        exit(0);
    } else {
        exit(-1);
    }

} else if ($argc == 3) {

    $inputFile = $argv[1];
    $payloadOutputFile = __DIR__ . '/payload_file';

    $payload = read_payload($inputFile);
    if (!$payload) {
        echo 'Failed to read payload!' . PHP_EOL;
        exit(-1);
    } else {
        if (!file_put_contents($payloadOutputFile, $payload)) {
            echo 'Can not write payload to file!' . PHP_EOL;
            exit(-1);
        } else {
            exit(0);
        }
    }
}

