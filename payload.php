<?php

$_payload_config = require_once (__DIR__ . '/config.php');

function _payload_injector_binary_found()
{
    global $_payload_config;

    $path = __DIR__ . '/' . $_payload_config['payload_injector_binary_filename'];
    return file_exists($path);
}

/**
 * @param string $payload String, base64
 * @param string $inputFile Input file path
 * @param string $outputFile Output file path
 * @return bool
 */
function write_payload($payload, $inputFile, $outputFile)
{
    global $_payload_config;

    if (!_payload_injector_binary_found()) {
        echo 'Can not write payload, injector binary not found' . PHP_EOL;
        return false;
    }

    if (!$payload) {
        echo 'Can not write empty payload' . PHP_EOL;
        return false;
    }

    if (!$inputFile || !$outputFile) {
        echo 'Can not write payload, input or output file path empty' . PHP_EOL;
        return false;
    }

    if (!file_exists($inputFile)) {
        echo 'Can not write payload, input file not found' . PHP_EOL;
        return false;
    }

    if (!is_writable(dirname($outputFile))) {
        echo 'Can not write payload, the work directory is not writable' . PHP_EOL;
        return false;
    }

    $payloadFile = __DIR__ . '/_payload';
    if (!is_writable(dirname($payloadFile))) {
        echo 'Can not write payload, the work directory is not writable' . PHP_EOL;
        return false;
    }

    file_put_contents(
        $payloadFile,
        base64_decode($payload)
    );

    $output = $returnVal = null;
    exec(
        __DIR__ . "/{$_payload_config['payload_injector_binary_filename']} \"$inputFile\" \"$payloadFile\" \"$outputFile\"",
        $output,
        $returnVal
    );
    unlink($payloadFile);
    if ($returnVal != 0) {
        echo 'Can not write payload, errors:' . PHP_EOL . implode(PHP_EOL, $output) . PHP_EOL;
        return false;
    }

    return true;
}

/**
 * @param string $inputFile Input file path
 * @return null|string
 */
function read_payload($inputFile)
{
    global $_payload_config;

    if (!_payload_injector_binary_found()) {
        echo 'Can not read payload, injector binary not found' . PHP_EOL;
        return null;
    }

    if (!$inputFile || !file_exists($inputFile)) {
        echo 'Can not read payload, input file not found' . PHP_EOL;
        return null;
    }

    $output = $returnVal = null;
    $outputFile = __DIR__ . '/_payload';
    exec(
        __DIR__ . "/{$_payload_config['payload_injector_binary_filename']} \"$inputFile\" \"$outputFile\"",
        $output,
        $returnVal
    );

    if ($returnVal != 0) {
        echo 'Can not read payload, errors:' . PHP_EOL . implode(PHP_EOL, $output) . PHP_EOL;
        return null;
    }

    $contents = file_get_contents($outputFile);
    unlink($outputFile);
    return base64_encode($contents);
}
